const getters = {
    token: state => state.admin.token,
    id: state => state.admin.id,
    username: state => state.admin.username
}
export default getters;