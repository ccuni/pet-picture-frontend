import Vue from 'vue'
import Vuex from 'vuex'
import getters from '@/store/getters.js'
import admin from '@/store/modules/admin.js'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    admin
  },
  getters
})

export default store
