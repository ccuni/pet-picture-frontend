import Vue from 'vue';

import VueRouter from 'vue-router'

Vue.use(VueRouter)
const router = new VueRouter({
    mode: 'hash',
    routes:[
        {
            path: '/',
            redirect: '/home',
        },
        {
            path: '/home',
            component: () => import('@/views/Home.vue')
        },
        { 
            path: '/search', 
            name: 'Search',
            query: {
                detail: '',             // 图片描述
                tag: '',                // 图片标签
                desc: false,
                startIndex: 0,
                t: '',                  // 随机字符串
            },
            component: () => import('@/views/Search.vue')
        },
        {
            path: '/tag',
            component: () => import('@/views/Tag.vue')
        }
    ]
})
export default router;