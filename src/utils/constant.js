// HTTP 请求代码枚举
export const HttpCodeEnum = {
    200: 'SUCCESS',        // 请求成功
    201: 'OK',             // 操作成功
    400: 'FAIL',           // 请求失败
    401: 'NOAUTH',         // 权限不足
    404: 'LOST',           // 找不到系统资源
    SUCCESS: 200,  
    OK: 201,     // 响应错误
    FAIL: 400,   // 请求失败
    NOAUTH: 401,    // 无权限
    LOST: 404       // 未找到系统资源
}
