export default {
    getImageInfo: function(imageUrl, callback) {
        var img = new Image();
        img.onload = function() {
            var width = this.width;
            var height = this.height;
            var ratio = width / height;
            if (callback && typeof callback === 'function') {
                callback({
                    width: width,
                    height: height,
                    ratio: ratio
                });
            }
        };
        img.src = imageUrl;
    },
    // 获取JSON对象中的url
    getImageUrlByArray(imageUrlArray){
        var result =  new Array()
        imageUrlArray.forEach(e => {
            result.push(e.imgUrl)
        });
        return result
    }
};
