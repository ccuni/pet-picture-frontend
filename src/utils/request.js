import axios from 'axios'
import { MessageBox, Message } from 'element-ui'
import vm from '../main'
import store from '@/store'
import { getToken } from '@/utils/auth'
import { BASE_API } from '@/config/dev.env.js'

import { HttpCodeEnum } from './constant'
// 创建 Axios 实例
const service = axios.create({
  baseURL: BASE_API,                    // url = base url + request url
  timeout: 60000                        // 请求等待超时时间 1min
})

// 请求拦截
service.interceptors.request.use(
  config => {                           // 处理请求信息
    // 如果有 Token 则添加到请求头中
    if (store.getters.token) {
      config.headers['X-Token'] = getToken()
    }
    return config
  },
  error => {                            // 处理错误信息
    console.log(error)                  // DEBUG 
    return Promise.reject(error)
  }
)

// 响应拦截
service.interceptors.response.use(
  response => {
    const res = response.data
    if (res.code == HttpCodeEnum.NOAUTH) {   // 权限不足时提示用户进行登录
      Message({                        // 弹出提示框
        message: '没有操作权限, 请重新登录',     // 优先显示响应体中的 message, 若没有就显示 Error
        type: 'error',
        duration: 3 * 1000                   // 弹窗持续时间
      })
      // 切换到登录页面
      setTimeout(() => {
        vm.$router.push({ path: '/login' })
      }, 500)
    } else if (res.code == HttpCodeEnum.SUCCESS) { // 成功时直接返回
      return res
    } else if (res.code == HttpCodeEnum.FAIL || res.code == HttpCodeEnum.ERROR) {  // 操作失败
      Message({
        message: res.message || 'Error',
        type: 'error',
        duration: 3 * 1000
      })
      return Promise.reject(new Error(res.message || 'Error'))
    }
  },
  error => {
    Message({
      message: '服务器内部出错, 无法正常连接',
      type: 'error',
      duration: 3 * 1000
    })
    return Promise.reject(error)
  }
)

export default service
