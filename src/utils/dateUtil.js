import moment from 'moment';

export function dateUtilFormat(date) {
    return moment(date).format('YYYY年MM月DD日 HH:mm:ss');
}