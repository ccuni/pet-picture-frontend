import request from '@/utils/request';

export default {
    Login(userVO){
        return request({
            url: '/admin/login',
            method: 'post',
            data: userVO
        })
    },
    // 注销登录
    Logout(){
        return request({
            url: '/admin/logout',
            method: 'post',
        })
    },
    // 验证验证码是否正确
    verifyCaptcha(username, captcha){
        return request({
            url: `/captcha/${username}/${captcha}`,
            method: 'get',
        })
    },
    // 根据 token 获取信息
    GetInfo(token){
        return request({
            url: '/admin/token',
            method: 'post',
            data: token
        })
    },
    // 获取验证码图片
    GetCaptchaImage(usermame){
        return request({
            url: `/captcha/${usermame}`,
            method: 'get',
        })
    }
}

