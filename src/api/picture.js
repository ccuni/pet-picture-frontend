import request from '@/utils/request';

export default {
    /**
     * 条件查询图片
     * @param {*} condition 
     */
    queryPicture(condition){
        return request({
            url: `/picture?detail=${condition.detail}&tag=${condition.tag}&desc=${condition.desc}&startIndex=${condition.startIndex}`,
            method: 'get',
        })

    },
    /**
     * 条件统计图片数量
     */
    countPicture(condition){
        return request({
            url: `/picture/count?detail=${condition.detail}&tag=${condition.tag}`,
            method: 'get',
        })
    },
    /**
     * 查询图片标签
     */
    queryPicTag(){
        return request({
            url: `/picture/tag`,
            method: 'get',
        })
    }
}