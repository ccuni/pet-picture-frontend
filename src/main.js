import Vue from 'vue'
import App from './App.vue'
import router from '@/router/index.js'
Vue.config.productionTip = false;

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import animate from 'animate.css'

Vue.use(ElementUI);
Vue.use(animate);

const vm = new Vue({
  render: h => h(App),
  router
}).$mount('#app');

export default vm;